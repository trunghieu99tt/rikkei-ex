package com.example.rikkeiexx.models;

import com.example.rikkeiexx.models.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty(value = "id", access = JsonProperty.Access.READ_ONLY)
    @JsonIgnoreProperties
    @JsonIgnore
    private Long id;

    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private String address;
    private String city;
    private String state;
    private String email;
    private String employer;
    private Long accountNumber;
    private Long balance;


    @ManyToOne()
    @JoinColumn(name = "userID")
    private User user;

}
