package com.example.rikkeiexx.models.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountDTO {
    private String firstName;
    private String lastName;
    private String gender;
    private int age;
    private String address;
    private String city;
    private String state;
    private String email;
    private String employer;
    private Long accountNumber;
    private Long balance;
}
