package com.example.rikkeiexx.controller;

import com.example.rikkeiexx.common.exception.model.BackendError;
import com.example.rikkeiexx.common.response.ResponseTool;
import com.example.rikkeiexx.common.response.model.ApiPagingResponse;
import com.example.rikkeiexx.common.response.model.ApiResponse;
import com.example.rikkeiexx.models.BankAccount;
import com.example.rikkeiexx.models.user.User;
import com.example.rikkeiexx.models.user.dto.BankAccountDTO;
import com.example.rikkeiexx.service.BankAccountService;
import com.example.rikkeiexx.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/bankAccount")
@RequiredArgsConstructor
@Tag(name = "Bank Account")
@SecurityRequirement(name = "bearerAuth")
public class BankAccountController {

    @Autowired
    BankAccountService bankAccountService;

    @Autowired
    UserService userService;


    @GetMapping("")
    public ResponseEntity<ApiPagingResponse> getBankAccounts(
            @RequestParam(required = false, defaultValue = "8") int pageSize,
            @RequestParam(required = false, defaultValue = "1") int pageNumber,
            @RequestParam(required = false, defaultValue = "-1") Long accountNumber,
            @RequestParam(required = false, defaultValue = "-1") int age,
            @RequestParam(required = false, defaultValue = "1") int sortBy,
            @RequestParam(required = false, defaultValue = "true") boolean isAsc,
            @RequestParam(required = false) String address,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String employer,
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String gender,
            @RequestParam(required = false) String lastName,
            @RequestParam(required = false) String state
    ) {
        Page<BankAccount> bankAccounts = bankAccountService.getAllBankAccount(pageNumber, pageSize, sortBy, isAsc,
                accountNumber,
                address, age, city, email, employer, firstName, gender, lastName, state);
        return ResponseTool.GET_OK(new ArrayList<Object>(bankAccounts.getContent()),
                (int) bankAccounts.getTotalElements());
    }

    @ApiOperation(value = "Create a bank account")
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@EndpointAuthorizer.authorizer({'admin'})")
    public ResponseEntity<ApiResponse> createBankAccount(
            @Valid @RequestBody BankAccountDTO bankAccountDTO)
            throws BackendError {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());
        BankAccount newBankAccount = bankAccountService.add(bankAccountDTO, user);
        return ResponseTool.POST_OK(newBankAccount);
    }

    @ApiOperation(value = "Get bank account by id")
    @GetMapping("/{bankAccountID}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ApiResponse> getBankAccount(
            @PathVariable(name = "bankAccountID") long bankAccountID
    ) throws BackendError {
        BankAccount bankAccount = bankAccountService.getBankAccount(bankAccountID);
        if (bankAccount == null) {
            String message = "Invalid bank account ID";
            throw new BackendError(HttpStatus.BAD_REQUEST, message);
        }
        return ResponseTool.GET_OK(bankAccount);
    }

    @ApiOperation(value = "update a bank account")
    @PutMapping("/{bankAccountID}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@EndpointAuthorizer.authorizer({'admin'})")
    public ResponseEntity<ApiResponse> updateBankAccount(
            @PathVariable(name = "bankAccountID") long bankAccountID,
            @Valid @RequestBody BankAccount updatedBankAccount
    ) throws BackendError {
        BankAccount bankAccountDB = bankAccountService.getBankAccount(bankAccountID);
        if (bankAccountDB == null) {
            String message = "Invalid bank account ID";
            throw new BackendError(HttpStatus.BAD_REQUEST, message);
        }
        updatedBankAccount.setId(bankAccountDB.getId());
        updatedBankAccount.setUser(bankAccountDB.getUser());
        return ResponseTool.PUT_OK(bankAccountService.updateBankAccount(updatedBankAccount));
    }

    @ApiOperation(value = "delete a bank account")
    @DeleteMapping("/{bankAccountID}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("@EndpointAuthorizer.authorizer({'admin'})")
    public ResponseEntity<ApiResponse> deleteBankAccount(
            @PathVariable(name = "bankAccountID") long bankAccountID
    ) throws BackendError {
        boolean ok = bankAccountService.deleteBankAccount(bankAccountID);
        if (ok) {
            return ResponseTool.DELETE_OK();
        } else {
            throw new BackendError(HttpStatus.BAD_REQUEST, "There is no bank account  with this id");
        }
    }


}
