package com.example.rikkeiexx.controller;

import com.example.rikkeiexx.common.exception.model.BackendError;
import com.example.rikkeiexx.common.response.ResponseTool;
import com.example.rikkeiexx.common.response.model.ApiResponse;
import com.example.rikkeiexx.models.user.CustomUserDetail;
import com.example.rikkeiexx.models.user.User;
import com.example.rikkeiexx.models.user.dto.LoginDTO;
import com.example.rikkeiexx.models.user.dto.LoginResponse;
import com.example.rikkeiexx.service.UserService;
import com.example.rikkeiexx.utils.jwt.JwtTokenProvider;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/users")
@Tag(name = "User Controller")
public class UserController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    @Operation(description = "Login")
    public ResponseEntity<ApiResponse> login(@RequestBody LoginDTO user) throws BackendError {
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(),
                        user.getPassword()));
        SecurityContextHolder.getContext().
                setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken((CustomUserDetail) authentication.getPrincipal());
        UserDetails userDB = userService.loadUserByUsername(user.getUsername());
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setAccess_token(jwt);
        loginResponse.setRole(userDB.getAuthorities().toString());
        return ResponseTool.POST_OK(loginResponse);
    }

    @PostMapping("/register")
    @Operation(description = "create a user")
    public ResponseEntity<ApiResponse> register(@Valid @RequestBody User user) throws BackendError {
        User newUser = userService.createUser(user);
        System.out.println("new user" + newUser.toString());
        return ResponseTool.POST_OK(newUser);
    }

    @GetMapping("/getMe")
    @Operation(description = "Get profile", summary = "Get profile", security =
    @SecurityRequirement(name = "bearerAuth"))
    @PreAuthorize("@EndpointAuthorizer.authorizer({'admin', 'normal'})")
    public ResponseEntity<ApiResponse> getMe() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(authentication.getName());
        return ResponseTool.GET_OK(user);
    }
}
