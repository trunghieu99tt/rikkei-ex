package com.example.rikkeiexx.service.specifiation;

import com.example.rikkeiexx.models.BankAccount;
import com.example.rikkeiexx.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountSpecification implements Specification<BankAccount> {

    private int age;
    private String city;
    private String state;
    private String email;
    private String gender;
    private String employer;
    private String address;
    private String lastName;
    private String firstName;
    private Long accountNumber;

    private int sortCase;
    private boolean isAscSort;

    @Override
    public Predicate toPredicate(Root<BankAccount> root, CriteriaQuery<?> criteriaQuery,
                                 CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new LinkedList<>();

        if (age != -1) {
            predicateList.add(criteriaBuilder.equal(root.get("age"), age));
        }

        if (city != null && !city.trim().isEmpty()) {
            predicateList.add(criteriaBuilder.like(root.get("city"), "%" + city + "%"));
        }

        if (state != null && !state.trim().isEmpty()) {
            System.out.println("sate " + state);
            predicateList.add(criteriaBuilder.like(root.get("state"), "%" + state + "%"));
        }

        if (email != null && !email.trim().isEmpty()) {
            predicateList.add(criteriaBuilder.like(root.get("email"), "%" + email + "%"));
        }

        if (gender != null) {
            System.out.println("gender " + gender);
            predicateList.add(criteriaBuilder.like(root.get("gender"), gender));
        }

        if (employer != null && !employer.trim().isEmpty()) {
            predicateList.add(criteriaBuilder.like(root.get("employer"), "%" + employer + "%"));
        }

        if (address != null && !address.trim().isEmpty()) {
            predicateList.add(criteriaBuilder.like(root.get("address"), "%" + address + "%"));
        }

        if (lastName != null && !lastName.trim().isEmpty()) {
            System.out.println("lastName " + lastName);
            predicateList.add(criteriaBuilder.like(root.get("lastName"), "%" + lastName + "%"));
        }

        if (firstName != null && !firstName.trim().isEmpty()) {
            predicateList.add(criteriaBuilder.like(root.get("firstName"), "%" + firstName + "%"));
        }

        if (accountNumber != -1) {
            predicateList.add(criteriaBuilder.equal(root.get("accountNumber"), accountNumber));
        }

        // SORT
        Path orderClause;
        switch (sortCase) {
            case Constant
                    .SORT_BY_ACCOUNT_NUMBER:
                orderClause = root.get("accountNumber");
                break;
            case Constant.SORT_BY_AGE:
                orderClause = root.get("age");
                break;
            case Constant.SORT_BY_ADDRESS:
                orderClause = root.get("address");
                break;
            case Constant.SORT_BY_BALANCE:
                orderClause = root.get("balance");
                break;
            case Constant.SORT_BY_CITY:
                orderClause = root.get("city");
                break;
            case Constant.SORT_BY_LAST_NAME:
                orderClause = root.get("lastName");
                break;
            case Constant.SORT_BY_STATE:
                orderClause = root.get("state");
                break;
            default:
                orderClause = root.get("firstName");
        }

        if (isAscSort) {
            criteriaQuery.orderBy(criteriaBuilder.asc(orderClause));
        } else {
            criteriaQuery.orderBy(criteriaBuilder.desc(orderClause));
        }

        return criteriaBuilder.and(predicateList.toArray(new Predicate[]{}));
    }
}
