package com.example.rikkeiexx.service;

import com.example.rikkeiexx.common.exception.model.BackendError;
import com.example.rikkeiexx.models.BankAccount;
import com.example.rikkeiexx.models.user.User;
import com.example.rikkeiexx.models.user.dto.BankAccountDTO;
import com.example.rikkeiexx.repository.BankAccountRepository;
import com.example.rikkeiexx.service.specifiation.BankAccountSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BankAccountService {

    BankAccountRepository bankAccountRepository;

    @Autowired
    BankAccountService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    /**
     * @param pageNumber    page number
     * @param pageSize      number of record per page
     * @param accountNumber account number
     * @param address       address
     * @param age           age
     * @param city          city
     * @param email         email
     * @param employer      employer
     * @param firstName     first name
     * @param gender        gender
     * @param lastName      last name
     * @param state         state
     * @return Page<BankAccount />
     */
    public Page<BankAccount> getAllBankAccount(
            int pageNumber,
            int pageSize,
            int sortBy,
            boolean isAsc,
            Long accountNumber,
            String address,
            int age,
            String city,
            String email,
            String employer,
            String firstName,
            String gender,
            String lastName,
            String state
    ) {
        Pageable pageable = PageRequest.of(pageNumber - 1, pageSize);
        BankAccountSpecification bankAccountSpecification = new BankAccountSpecification(
                age, city, state, email, gender, employer, address, lastName, firstName, accountNumber, sortBy, isAsc
        );
        return bankAccountRepository.findAll(bankAccountSpecification, pageable);
    }

    /**
     * @param bankAccountDTO new bank account
     * @param user
     * @return Bank Account
     */
    public BankAccount add(BankAccountDTO bankAccountDTO, User user) throws BackendError {
        if (bankAccountRepository.getBankAccountByAccountNumber(bankAccountDTO.getAccountNumber()).size() > 0) {
            throw new BackendError(HttpStatus.BAD_REQUEST, "Account number does exist! Please choose another bank " +
                    "account number");
        }
        if (bankAccountRepository.getBankAccountByEmail(bankAccountDTO.getEmail()).size() > 0) {
            throw new BackendError(HttpStatus.BAD_REQUEST, "Email does exist! Please choose another email");
        }
        BankAccount bankAccount = convertDToToModel(bankAccountDTO);
        bankAccount.setUser(user);
        return bankAccountRepository.save(bankAccount);
    }

    /**
     * @param bankAccountID id of bank account
     * @return Bank Account
     */
    public BankAccount getBankAccount(Long bankAccountID) {
        Optional<BankAccount> bankAccount = bankAccountRepository.findById(bankAccountID);
        return bankAccount.orElse(null);
    }

    /**
     * @param bankAccountID id of bank account
     * @return true if delete successfully, false otherwise
     */
    public boolean deleteBankAccount(Long bankAccountID) {
        if (getBankAccount(bankAccountID) != null) {
            bankAccountRepository.deleteById(bankAccountID);
            return true;
        }
        return false;
    }

    /**
     * @param updatedBankAccount updated bank account
     * @return Bank Account
     */
    public BankAccount updateBankAccount(BankAccount updatedBankAccount) {
        return bankAccountRepository.save(updatedBankAccount);
    }

    private BankAccount convertDToToModel(BankAccountDTO bankAccountDTO) {
        BankAccount res = new BankAccount();
        res.setAccountNumber(bankAccountDTO.getAccountNumber());
        res.setAddress(bankAccountDTO.getAddress());
        res.setFirstName(bankAccountDTO.getFirstName());
        res.setLastName(bankAccountDTO.getLastName());
        res.setAge(bankAccountDTO.getAge());
        res.setAddress(bankAccountDTO.getAddress());
        res.setCity(bankAccountDTO.getCity());
        res.setState(bankAccountDTO.getState());
        res.setGender(bankAccountDTO.getGender());
        res.setEmail(bankAccountDTO.getEmail());
        res.setBalance(bankAccountDTO.getBalance());
        res.setEmployer(bankAccountDTO.getEmployer());
        return res;
    }

}
