package com.example.rikkeiexx.utils;

public class Constant {

    // define sort key value for bank account
    public static final int SORT_BY_FIRST_NAME = 1;
    public static final int SORT_BY_LAST_NAME = 2;
    public static final int SORT_BY_AGE = 3;
    public static final int SORT_BY_CITY = 4;
    public static final int SORT_BY_STATE = 5;
    public static final int SORT_BY_BALANCE = 6;
    public static final int SORT_BY_ADDRESS = 7;
    public static final int SORT_BY_ACCOUNT_NUMBER = 8;

}
