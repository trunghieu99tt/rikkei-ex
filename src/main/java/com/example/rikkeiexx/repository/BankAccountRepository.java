package com.example.rikkeiexx.repository;

import com.example.rikkeiexx.models.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long>, JpaSpecificationExecutor<BankAccount> {
    List<BankAccount> getBankAccountByAccountNumber(Long accountNumber);

    List<BankAccount> getBankAccountByEmail(String email);
}
