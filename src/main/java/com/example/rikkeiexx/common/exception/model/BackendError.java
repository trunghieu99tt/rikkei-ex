package com.example.rikkeiexx.common.exception.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BackendError extends Exception {
    private HttpStatus statusCode;
    private String message;
}
