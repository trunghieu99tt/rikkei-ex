package com.example.rikkeiexx.common.response;

import com.example.rikkeiexx.common.response.model.ApiPagingResponse;
import com.example.rikkeiexx.common.response.model.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ResponseTool<T> {

    public static ResponseEntity<ApiResponse> POST_OK(Object data) {
        return new ResponseEntity<>(new ApiResponse(201, "Created", data), HttpStatus.CREATED);
    }

    public static ResponseEntity<ApiResponse> DELETE_OK() {
        return new ResponseEntity<>(new ApiResponse(200, "Deleted", null), HttpStatus.OK);
    }

    public static ResponseEntity<ApiResponse> GET_OK(Object data) {
        return new ResponseEntity<>(new ApiResponse(200, "Get ok", data), HttpStatus.OK);
    }

    public static ResponseEntity<ApiPagingResponse> GET_OK(List<Object> data, int total) {
        return new ResponseEntity<>(new ApiPagingResponse(200, "Get OK", data, total), HttpStatus.OK);
    }

    public static ResponseEntity<ApiResponse> PUT_OK(Object data) {
        return new ResponseEntity<>(new ApiResponse(200, "Put", data), HttpStatus.OK);
    }

}
