package com.example.rikkeiexx.common.response.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse {

    @NotNull
    @NotBlank
    private int status;
    private String message;
    private Object data;
}
