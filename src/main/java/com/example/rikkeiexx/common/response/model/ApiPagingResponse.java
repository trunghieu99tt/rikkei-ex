package com.example.rikkeiexx.common.response.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiPagingResponse {

    private int status;
    private String message;
    private List<Object> data;
    private int total;

}
